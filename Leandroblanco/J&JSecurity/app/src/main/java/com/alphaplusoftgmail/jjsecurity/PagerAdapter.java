package com.alphaplusoftgmail.jjsecurity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:

                negocio_fragment tab1 = new negocio_fragment();
                return tab1;
            case 1:
                zonas_fragment tab2 = new zonas_fragment();
                //calcular_fragment tab2 = new calcular_fragment();
                return tab2;
           /* case 2:
                //cuotas_fragment tab3 = new cuotas_fragment();
                historial_fragment tab3 = new historial_fragment();
                return tab3;*/
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}