package com.alphaplusoftgmail.jjsecurity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alphaplusoftgmail.jjsecurity.Control.AppController;
import com.alphaplusoftgmail.jjsecurity.logica.negocio_class;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.R.attr.value;

public class MainActivity extends AppCompatActivity {

    static Button iniciar;
    private EditText usuario;
    private EditText password;

    private String urljsondata ;

    private static String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onTokenRefresh();

        iniciar = (Button) this.findViewById(R.id.iniciar_sesion);
        usuario = (EditText) this.findViewById(R.id.usuario);
        password = (EditText) this.findViewById(R.id.password);

        iniciar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //String user = "juangt";
                //String pass = "yaroa";
                String usuariox = usuario.getText().toString();
                String passwordx = password.getText().toString();

                urljsondata =  "https://jjs.herokuapp.com/api/checre/?use="+usuariox+"&pas="+passwordx+"&format=json";

                verifyLogin();


             /*   if(usuariox.equals(user)  && passwordx.equals(pass)){

                    Toast.makeText(getApplicationContext(),
                            "DATOS CORRECTOS",
                            Toast.LENGTH_LONG).show();

                    Intent Tab_activity = new Intent(getBaseContext(), contenido.class);
                    Tab_activity.putExtra("user", user); //Optional parameters
                    startActivity(Tab_activity);
               }
                else
                {
                    Toast.makeText(getApplicationContext(),
                            "Error: " + "DATOS DE SESION INVALIDOS",
                            Toast.LENGTH_LONG).show();
                }*/
            }
        });
    }


    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }

    public void sendRegistrationToServer(String token){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("token_valido");

        myRef.setValue(token);
    }

    private void verifyLogin() {

        // showpDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urljsondata, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                Log.d(TAG,urljsondata);

                try {

                    JSONArray negocio = response.getJSONArray("objects");
                    JSONObject socio = (JSONObject) negocio.get(0);

                    Log.d(TAG, socio.toString());

                    String estado = socio.getString("Estado");
                    String idnegocio = socio.getString("Negocioid");
                    String numero = socio.getString("Emergencia");


                    if(estado.equals("OK")){

                        Toast.makeText(getApplicationContext(),
                                "DATOS CORRECTOS",
                                Toast.LENGTH_LONG).show();

                        Intent next_activity = new Intent(getBaseContext(), contenido.class);
                        next_activity.putExtra("idnegocio", idnegocio); //Optional parameters
                        next_activity.putExtra("numero", numero); //Optional parameters
                        startActivity(next_activity);
                    }else
                    {

                        Toast.makeText(getApplicationContext(),
                                "Error: " + "DATOS DE SESION INVALIDOS",
                                Toast.LENGTH_LONG).show();
                    }




                    Toast.makeText(getBaseContext(),
                            "TERMINADO: " + "DATA ADQUIRIDA",
                            Toast.LENGTH_LONG).show();




                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                    // txtResponse.setText(e.getMessage());

                }
                // hidepDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getBaseContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                // hide the progress dialog
                // hidepDialog();

                //  txtResponse.setText(error.getMessage());

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

}
