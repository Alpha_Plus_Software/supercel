package com.alphaplusoftgmail.jjsecurity.logica;

/**
 * Created by No. GP62 on 17/07/2017.
 */

public class negocio_class {

    private String latitud;
    private String longitud;
    private String nombre;
    private String sensores;
    private String servidores;

    public negocio_class(String latitud, String longitud, String nombre, String sensores, String servidores) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.nombre = nombre;
        this.sensores = sensores;
        this.servidores = servidores;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSensores() {
        return sensores;
    }

    public void setSensores(String sensores) {
        this.sensores = sensores;
    }

    public String getServidores() {
        return servidores;
    }

    public void setServidores(String servidores) {
        this.servidores = servidores;
    }
}
