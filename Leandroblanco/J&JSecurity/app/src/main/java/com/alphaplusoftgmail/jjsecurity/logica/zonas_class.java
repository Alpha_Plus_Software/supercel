package com.alphaplusoftgmail.jjsecurity.logica;

/**
 * Created by No. GP62 on 17/07/2017.
 */

public class zonas_class {

    private String id;
    private String nombre;
    private String estadomov;
    private String estadoson;

    public zonas_class(String id, String nombre, String estadomov, String estadoson) {
        this.id = id;
        this.nombre = nombre;
        this.estadomov = estadomov;
        this.estadoson = estadoson;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstadomov() {
        return estadomov;
    }

    public void setEstadomov(String estadomov) {
        this.estadomov = estadomov;
    }

    public String getEstadoson() {
        return estadoson;
    }

    public void setEstadoson(String estadoson) {
        this.estadoson = estadoson;
    }
}
