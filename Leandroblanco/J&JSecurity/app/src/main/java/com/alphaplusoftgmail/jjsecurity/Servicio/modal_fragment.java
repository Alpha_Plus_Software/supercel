package com.alphaplusoftgmail.jjsecurity.Servicio;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alphaplusoftgmail.jjsecurity.Control.AppController;
import com.alphaplusoftgmail.jjsecurity.MainActivity;
import com.alphaplusoftgmail.jjsecurity.R;
import com.alphaplusoftgmail.jjsecurity.logica.zonas_class;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.alphaplusoftgmail.jjsecurity.contenido.idnegocio;
import static com.alphaplusoftgmail.jjsecurity.contenido.numero;
import static com.alphaplusoftgmail.jjsecurity.zonas_fragment.Listazonas;
import static com.alphaplusoftgmail.jjsecurity.zonas_fragment.adapter;

/**
 * Created by No. GP62 on 28/07/2017.
 */

public class modal_fragment extends BottomSheetDialogFragment {

    private static String TAG = MainActivity.class.getSimpleName();
    private Button llamar;
    private Button zona_segura;
    private TextView nombre;
    private String urljsondata;
    String mString;
    String mid;

    public static modal_fragment newInstance(String string) {
        modal_fragment f = new modal_fragment();
        Bundle args = new Bundle();
        args.putString("string", string);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String intermedio =getArguments().getString("string");
       // Toast.makeText(getActivity().getApplicationContext(), intermedio, Toast.LENGTH_LONG).show();
        String[] parts = intermedio.split("-");
        mString = parts[0];
       // Toast.makeText(getActivity().getApplicationContext(), parts[0], Toast.LENGTH_LONG).show();
        mid = parts[1];
       // Toast.makeText(getActivity().getApplicationContext(), parts[1], Toast.LENGTH_LONG).show();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.modal_layout, container, false);

        llamar = (Button) v.findViewById(R.id.llamar);
        zona_segura = (Button) v.findViewById(R.id.verificado);
        nombre = (TextView) v.findViewById(R.id.nombre_zona);

        nombre.setText(mString);


        llamar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse(numero));

                Log.d(TAG,"CLICK ECHO");
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                    Log.d(TAG,"SIN PERMISOS");
                    return;
                }
                startActivity(callIntent);

            }
        });

        zona_segura.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {


                urljsondata = "https://jjs.herokuapp.com/api/eavalor/?sound=True&mod=True&ida="+ mid +"&format=json";
                Log.d(TAG,"INTENTANDO CAMBIAR ESTADO");
                cambiar_estado();

            }
        });

        return v;
    }

    /**
     * Method to make json object request where json response starts wtih {
     * */
    private void cambiar_estado() {



        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urljsondata, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                System.out.println("ENTRO");

               // Listazonas.notifyDataSetChanged();

                Listazonas.setAdapter(adapter);
                adapter.notifyDataSetChanged();





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),error.getMessage(), Toast.LENGTH_LONG).show();


            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }
}
