package com.alphaplusoftgmail.jjsecurity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alphaplusoftgmail.jjsecurity.Control.AppController;
import com.alphaplusoftgmail.jjsecurity.logica.negocio_class;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.alphaplusoftgmail.jjsecurity.contenido.idnegocio;

/**
 * Created by No. GP62 on 21/07/2017.
 */

public class negocio_fragment extends android.support.v4.app.Fragment{


    // json object response url
    private String urljsondata = "https://jjs.herokuapp.com/api/negocio/?idne="+idnegocio+"&format=json";

    private static String TAG = MainActivity.class.getSimpleName();


    static negocio_class data_negocio;
    // Progress dialog
    private ProgressDialog pDialog;

    private TextView valor1;
    private TextView titulo1;

    private TextView valor2;
    private TextView titulo2;

    private TextView valor3;
    private TextView titulo3;

    private TextView valor4;
    private TextView titulo4;

    private TextView valor5;
    private TextView titulo5;




    // temporary string to show the parsed response
    private String jsonResponse;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View vista = inflater.inflate(R.layout.negocio_fragment_view, container, false);

        // txtResponse = (TextView) vista.findViewById(R.id.error);


        titulo1= (TextView) vista.findViewById(R.id.titulo1);
        titulo2= (TextView) vista.findViewById(R.id.titulo2);
        titulo3= (TextView) vista.findViewById(R.id.titulo3);
        titulo4= (TextView) vista.findViewById(R.id.titulo4);
        titulo5= (TextView) vista.findViewById(R.id.titulo5);

        valor1 = (TextView) vista.findViewById(R.id.valor1);
        valor2 = (TextView) vista.findViewById(R.id.valor2);
        valor3 = (TextView) vista.findViewById(R.id.valor3);
        valor4 = (TextView) vista.findViewById(R.id.valor4);
        valor5 = (TextView) vista.findViewById(R.id.valor5);


        makeJsonObjectRequest();

        if(data_negocio != null) {

            valor1.setText(data_negocio.getNombre());
            titulo1.setText("NEGOCIO");

            valor2.setText(data_negocio.getSensores());
            titulo2.setText("SENSORES");

            valor3.setText(data_negocio.getServidores());
            titulo3.setText("SERVIDORES");

            valor4.setText(data_negocio.getLatitud());
            titulo4.setText("LATITUD");

            valor5.setText(data_negocio.getLongitud());
            titulo5.setText("LONGITUD");
        }


        return vista;
    }


    private void makeJsonObjectRequest() {

        // showpDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urljsondata, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {

                    JSONArray negocio = response.getJSONArray("objects");
                    JSONObject socio = (JSONObject) negocio.get(0);

                    String laltitud = socio.getString("latitud");
                    String longitud = socio.getString("longitud");
                    String nombre = socio.getString("nombre");
                    String sensores = socio.getString("sensores");
                    String servidores = socio.getString("servidores");

                    data_negocio = new negocio_class(laltitud,longitud,nombre,sensores,servidores);

                    valor1.setText(data_negocio.getNombre());
                    titulo1.setText("NEGOCIO");

                    valor2.setText(data_negocio.getSensores());
                    titulo2.setText("SENSORES");

                    valor3.setText(data_negocio.getServidores());
                    titulo3.setText("SERVIDORES");

                    valor4.setText(data_negocio.getLatitud());
                    titulo4.setText("LATITUD");

                    valor5.setText(data_negocio.getLongitud());
                    titulo5.setText("LONGITUD");

                 /*   Toast.makeText(getActivity().getApplicationContext(),
                            "TERMINADO: " + "DATA ADQUIRIDA",
                            Toast.LENGTH_LONG).show();*/
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                    // txtResponse.setText(e.getMessage());

                }
                // hidepDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                // hide the progress dialog
                // hidepDialog();

                //  txtResponse.setText(error.getMessage());

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);



    }
}
