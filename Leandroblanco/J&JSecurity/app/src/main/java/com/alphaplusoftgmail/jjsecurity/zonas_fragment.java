package com.alphaplusoftgmail.jjsecurity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alphaplusoftgmail.jjsecurity.Control.AppController;
import com.alphaplusoftgmail.jjsecurity.Servicio.modal_fragment;
import com.alphaplusoftgmail.jjsecurity.logica.zonas_class;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.alphaplusoftgmail.jjsecurity.contenido.idnegocio;

/**
 * Created by No. GP62 on 23/07/2017.
 */

public class zonas_fragment extends android.support.v4.app.Fragment {

    private ArrayList<zonas_class> zonas = new ArrayList<>();

    private String urljsondata = "https://jjs.herokuapp.com/api/modulo/?idne="+idnegocio+"&format=json";

    private static String TAG = MainActivity.class.getSimpleName();

    public static ArrayAdapter<zonas_class> adapter;

    public static ListView Listazonas;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View vista = inflater.inflate(R.layout.modulos_fragment_view, container, false);

        // txtResponse = (TextView) vista.findViewById(R.id.error);



        makeJsonObjectRequest();


        adapter = new zonaArrayAdapter(getActivity().getApplicationContext(),0, zonas);
        Listazonas = (ListView) vista.findViewById(R.id.modulos_lsit);



        Listazonas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                // prestamos_estructura selItem = (prestamos_estructura) adapter.getItemAtPosition(position);

                zonas_class s =(zonas_class) Listazonas.getItemAtPosition(position);


                String k = String.valueOf(s.getNombre()) + "-" + String.valueOf(s.getId())  ;

             //  Toast.makeText(getActivity().getApplicationContext(), k, Toast.LENGTH_LONG).show();
                BottomSheetDialogFragment myBottomSheet  =  modal_fragment.newInstance(k);
                myBottomSheet.show(getFragmentManager(), myBottomSheet.getTag());

            }
        });




        return vista;
    }

    //custom ArrayAdapter
    class zonaArrayAdapter extends ArrayAdapter<zonas_class> {

        private Context context;
        private List<zonas_class> zonass;

        //constructor, call on creation
        public zonaArrayAdapter(Context context, int resource, ArrayList<zonas_class> objects) {
            super(context, resource, objects);

            this.context = context;
            this.zonass = objects;
        }


        //called when rendering the list
        public View getView(int position, View convertView, ViewGroup parent) {

            //get the property we are displaying
            zonas_class informacion_actual = zonass.get(position);

            //get the inflater and inflate the XML layout for each item
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.modulo_item, null);

             TextView nombre = (TextView) view.findViewById(R.id.nombre);
            TextView movimiento = (TextView) view.findViewById(R.id.movimiento);
            TextView sonido = (TextView) view.findViewById(R.id.sonido);


            nombre.setText( String.valueOf(informacion_actual.getNombre()));
            movimiento.setText("Movimiento");
            sonido.setText( "Sonido");

            if(informacion_actual.getEstadomov().equals("true")){
                movimiento.setBackgroundColor(getResources().getColor(R.color.correcto));
            }
            else
            {
                movimiento.setBackgroundColor(getResources().getColor(R.color.problemas));
            }

            if(informacion_actual.getEstadoson().equals("true")){
                sonido.setBackgroundColor(getResources().getColor(R.color.correcto));
            }
            else
            {
                sonido.setBackgroundColor(getResources().getColor(R.color.problemas));
            }


            return view;
        }
    }


    /**
     * Method to make json object request where json response starts wtih {
     * */
    private void makeJsonObjectRequest() {



        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urljsondata, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {

                    JSONArray objeto = response.getJSONArray("objects");
                    JSONArray modulos = (JSONArray) objeto.get(0);
                    //JSONArray module = (JSONArray) modulos.get("modulos");

                    System.out.println(modulos.toString());

                    for (int i=0; i <modulos.length(); i++) {


                        JSONObject modulo_sel = (JSONObject) modulos.get(i);


                        int id = modulo_sel.getInt("id");
                        String nombre = modulo_sel.getString("nombre");
                        Boolean sensormov = modulo_sel.getBoolean("sensormov");
                        Boolean sensorson = modulo_sel.getBoolean("sensorson");

                        String idd = String.valueOf(id);
                        String senmov = String.valueOf(sensormov);
                        String senson = String.valueOf(sensorson);

                        zonas.add(new zonas_class(idd,nombre,senmov,senson));

                    }


                    Listazonas.setAdapter(adapter);

                 /*   Toast.makeText(getActivity().getApplicationContext(),
                            "TERMINADO: " + "DATA ADQUIRIDA",
                            Toast.LENGTH_LONG).show();*/
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                    // txtResponse.setText(e.getMessage());

                }
                // hidepDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                // hide the progress dialog
                // hidepDialog();

                //  txtResponse.setText(error.getMessage());

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }



}
